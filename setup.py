from setuptools import setup

setup(
   name='elasticscroll',
   version='1.0',
   description='Query elastic search via scroll-api',
   author='Fernando T',
   author_email='fernando.filho@mutantbr.com',
   packages=['elasticscroll'],  
   install_requires=['requests'], 
)